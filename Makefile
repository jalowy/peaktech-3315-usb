.PHONY: all peaktech3315 he2325u

all: peaktech3315 he2325u

peaktech3315:
	make -C src/peaktech
	cp src/peaktech/peaktech3315 ./

he2325u:
	make -C src/he2325u
	cp src/he2325u/he2325u ./

clean:
	make -C src/peaktech clean
	make -C src/he2325u clean
	rm peaktech3315 he2325u
